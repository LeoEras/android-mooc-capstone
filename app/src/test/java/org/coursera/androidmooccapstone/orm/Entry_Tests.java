package org.coursera.androidmooccapstone.orm;

// imports

import org.coursera.androidmooccapstone.framework.orm.AuthorEntry;
import org.coursera.androidmooccapstone.framework.orm.PaperEntry;
import org.coursera.androidmooccapstone.itemclass.Author;
import org.coursera.androidmooccapstone.provider.PaperProvider;
import org.junit.Before;
import org.junit.Test;

import shared.Entry_Utils;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

//@RunWith(JUnit4.class)
public class Entry_Tests {

    private PaperEntry mEntryA;
    private AuthorEntry mEntryB;

    private static PaperEntry newA() {
        return Entry_Utils.newPaperEntry();
    }

    static private AuthorEntry newB() {
        return Entry_Utils.newAuthorEntry();
    }

    @Before
    public void setUp() {
        mEntryA = newA();
        mEntryB = newB();
    }

    /*
     * Evaluating the creation of Paper and Author Entries
     */
    @Test
    public void equalsTest() {
        PaperEntry testEntry = newA();
        AuthorEntry testEntry2 = newB();
        Entry_Utils.checkPaperEntry(testEntry, Entry_Utils.PAPER_TEST_ID);
        Entry_Utils.checkAuthorEntry(testEntry2, Entry_Utils.AUTHOR_TEST_ID);
        assertTrue(mEntryA.equals(testEntry));
        assertTrue(mEntryB.equals(testEntry2));
        assertTrue(testEntry.equals(mEntryA));
        assertTrue(testEntry2.equals(mEntryB));
    }

    /*
    * Evaluating both an Author and Paper getters and setters
    */
    @Test
    public void testGetAndSet() {

        //Paper example tests
        PaperEntry testEntry = newA();
        testEntry.set_ID(Entry_Utils.PAPER_TEST_ID + 30L);
        testEntry.setTITLE(Entry_Utils.TEST_TITLE + "new2");
        testEntry.setDATE(Entry_Utils.TEST_DATE + "new3");
        testEntry.setABS(Entry_Utils.TEST_ABS + "new4");
        testEntry.setDOI(Entry_Utils.TEST_DOI + "new5");
        assertThat(testEntry.get_ID(), is(Entry_Utils.PAPER_TEST_ID + 30L));
        assertThat(testEntry.getTITLE(), is(Entry_Utils.TEST_TITLE + "new2"));
        assertThat(testEntry.getDATE(), is(Entry_Utils.TEST_DATE + "new3"));
        assertThat(testEntry.getABS(), is(Entry_Utils.TEST_ABS + "new4"));
        assertThat(testEntry.getDOI(), is(Entry_Utils.TEST_DOI + "new5"));


        //Author example tests
        AuthorEntry testEntry2 = newB();
        testEntry2.set_ID(Entry_Utils.AUTHOR_TEST_ID + 30L);
        testEntry2.setNAME(Entry_Utils.TEST_NAME + "new2");
        testEntry2.setLAST_NAME(Entry_Utils.TEST_LASTNAME + "new3");
        assertThat(testEntry2.get_ID(), is(Entry_Utils.AUTHOR_TEST_ID + 30L));
        assertThat(testEntry2.getNAME(), is(Entry_Utils.TEST_NAME + "new2"));
        assertThat(testEntry2.getLAST_NAME(), is(Entry_Utils.TEST_LASTNAME + "new3"));

    }

}
