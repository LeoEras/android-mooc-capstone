package shared;

import org.coursera.androidmooccapstone.framework.orm.AuthorEntry;
import org.coursera.androidmooccapstone.framework.orm.PaperEntry;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Shared Entry testing Utils class
 */
@SuppressWarnings("SameParameterValue")
public class Entry_Utils {

    static final public long PAPER_TEST_ID = 123456L;
    static final public String TEST_TITLE = "Lets party like its 1999";
    static final public String TEST_DATE = "20-02-2020";
    static final public String TEST_ABS = "An interesting abstract paragraph";
    static final public String TEST_DOI = "example_doi/202913.3021";

    static final public long AUTHOR_TEST_ID = 123457L;
    static final public String TEST_NAME = "Leonardo";
    static final public String TEST_LASTNAME = "Eras";

    static public PaperEntry newPaperEntry() {
        return new PaperEntry(PAPER_TEST_ID, TEST_TITLE, TEST_DATE, TEST_ABS, TEST_DOI);
    }

    static public AuthorEntry newAuthorEntry() {
        return new AuthorEntry(AUTHOR_TEST_ID , TEST_NAME, TEST_LASTNAME, PAPER_TEST_ID);
    }


    static public void checkPaperEntry(PaperEntry testEntry, long id) {
        assertThat(testEntry.get_ID(), is(id));
        assertThat(testEntry.getTITLE(), is(TEST_TITLE));
        assertThat(testEntry.getDATE(), is(TEST_DATE));
        assertThat(testEntry.getABS(), is(TEST_ABS));
        assertThat(testEntry.getDOI(), is(TEST_DOI));
    }

    static public void checkAuthorEntry(AuthorEntry testEntry, long id) {
        assertThat(testEntry.get_ID(), is(id));
        assertThat(testEntry.getNAME(), is(TEST_NAME));
        assertThat(testEntry.getLAST_NAME(), is(TEST_LASTNAME));
    }

}
