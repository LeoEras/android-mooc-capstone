package org.coursera.androidmooccapstone.services.utils;

import android.os.Bundle;

public interface ServiceResult {
    /**
     * Called when a launched Service sends back results from
     * computations it runs, giving the requestCode it was started
     * with, the resultCode it returned, and any additional data from
     * it.  The resultCode will be RESULT_CANCELED if the Service
     * explicitly returned that.
     */
    @SuppressWarnings("UnusedParameters")
    void onServiceResult(int requestCode,
                         int resultCode,
                         Bundle data
    );
}