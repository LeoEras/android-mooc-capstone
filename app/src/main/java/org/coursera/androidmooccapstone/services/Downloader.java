package org.coursera.androidmooccapstone.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;

import org.coursera.androidmooccapstone.framework.orm.PaperEntry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Downloader extends IntentService {
    private static final String TAG = Downloader.class.getCanonicalName();

    /**
     * String constant used to extract the Messenger "extra" from an
     * intent.
     */
    private static final String MESSENGER_KEY = "MESSENGER_KEY";

    /**
     * String constant used to extract the request code.
     */
    private static final String REQUEST_CODE = "REQUEST_CODE";

    /**
     * String constant used to extract the URL to an ATOM Feed from a Bundle.
     */
    private static final String FEED_URL = "FEED_URL";

    /**
     * String constant used to extract the parcelable array of Entry(s) from a Bundle.
     */
    private static final String ENTRY_ARRAY_KEY = "ENTRY_ARRAY_KEY";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public Downloader() {
        super("Downloader");
    }


    /**
     * Factory method that returns an explicit Intent for downloading an image.
     * <p>
     * This method is called by other components (Activities, etc.) to create an Intent that will
     * launch this Service. The reason it is done this way is to localize "KeyWord" variables
     * such as: REQUEST_CODE and MESSENGER_KEY, that way it increases the level of decoupling of
     * the code. (IE: Any change to those variables would impact many (any & all) class that
     * would launch this service if those variables were public and had to be used by the calling
     * component. However, as is, those variables are private and won't impact any other class if
     * they are changed.)
     */
    @SuppressWarnings("SameParameterValue")
    public static Intent makeIntent(Context context,
                                    int requestCode,
                                    Uri url,
                                    Handler downloadHandler) {
        // Create an intent that will download the image from the web.
        // which involves (1) setting the URL as "data" to the
        // intent, (2) putting the request code as an "extra" to the
        // intent, (3) creating and putting a Messenger as an "extra"
        // to the intent so the DownloadAtomFeedService can send the
        // Entry Object back to the Calling Activity
        // TODO -- you fill in here.
        return new Intent(context, Downloader.class)
                .setData(url)
                .putExtra(REQUEST_CODE, requestCode)
                .putExtra(MESSENGER_KEY, new Messenger(downloadHandler));
    }

    /**
     * Helper method to ease in using this class by localizing values needed for extracting data
     * from reply messages. Helps in getting Result Code.
     *
     * @param message message returned from this Service.
     * @return the result code.
     */
    public static int getResultCode(Message message) {
        // Check to see if the download succeeded.
        return message.arg1;
    }

    /**
     * Helper method to ease in using this class by localizing values needed for extracting data
     * from reply messages. Helps in getting Request Uri.
     *
     * @param message message returned from this Service.
     * @return the request Uri.
     */
    public static Uri getRequestUri(Message message) {
        // Extract the data from Message, which is in the form of a
        // Bundle that can be passed across processes.
        Bundle data = message.getData();
        // call getRequestUri(Bundle) on the data bundle and return the Uri it returns.
        return getRequestUri(data);
    }

    public static Uri getRequestUri(Bundle data) {
        // use 'FEED_URL' to extract the string representation of the Uri from the Message
        String url = data.getString(FEED_URL);
        // Parse the String of the url to get a Uri and return it.
        return Uri.parse(url);
    }

    /**
     * Helper method to ease in using this class by localizing values needed for extracting data
     * from reply messages. Helps in getting Request Code.
     *
     * @param message message returned from this Service.
     * @return the request code.
     */
    public static int getRequestCode(Message message) {
        // Extract the data from Message, which is in the form of a
        // Bundle that can be passed across processes.
        Bundle data = message.getData();
        // Extract the request code and return it.
        return data.getInt(REQUEST_CODE);
    }

    /**
     * Helper method to ease in using this class by localizing values needed for extracting data
     * from reply messages. Helps in getting the Entry(s) from the message.
     *
     * @param message message returned from this Service.
     * @return an ArrayList of the Entry(s) downloaded from the Uri.
     */
    public static String getDataDownloaded(@NonNull Message message) {
        // we give you this method since it isn't covered in full detail since it is beyond the
        // scope of this course.
        if (message.getData() == null) {
            return null;
        }
        return getDataDownloaded(message.getData());
    }

    /**
     * Helper method to ease in using this class by localizing values needed for extracting data
     * from reply messages. Helps in getting the Entry(s) from the Bundle.
     *
     * @param data the bundle that the data is in.
     * @return the ArrayList of Entry(s)
     */
    public static String getDataDownloaded(@NonNull Bundle data) {
        // we give you this method since it isn't covered in full detail since it is beyond the
        // scope of this course.
        return data.getString(ENTRY_ARRAY_KEY);
    }

    /**
     * Hook method dispatched by the IntentService framework to
     * download the image requested via data in an intent, store the
     * image in a local file on the local device, and return the image
     * file's URI back to the MainActivity via the Messenger passed
     * with the intent.
     */
    @Override
    public void onHandleIntent(Intent intent) {
        // Get the URL associated with the Intent data.
        Uri url = intent.getData();
        // Download the requested Paper Feed.
        String dataDownloaded = download(url.toString());
        // Extract the request code.
        int requestCode = (int) intent.getExtras().get(REQUEST_CODE);
        // Extract the Messenger stored as an extra in the
        // intent under the key MESSENGER_KEY.
        Messenger messenger = (Messenger) intent.getExtras().get(MESSENGER_KEY);
        // Send the Paper Entries back to the
        // MainActivity via the messenger.
        sendEntries(messenger, dataDownloaded, url, requestCode);
    }

    /**
     * Send the pathname back to the MainActivity via the
     * messenger.
     */
    private void sendEntries(Messenger messenger, String dataDownloaded,
                             Uri url,
                             int requestCode) {
        // Call the makeReplyMessage() factory method to create Message.
        Message message = makeReplyMessage(dataDownloaded, url, requestCode);

        try {
            // Send the path to the image file back to the MainActivity.
            messenger.send(message);

        } catch (RemoteException e) {
            Log.e(getClass().getName(),
                    "Exception while sending reply message back to Activity.",
                    e);
        }
    }

    /**
     * A factory method that creates a Message to return to the
     * MainActivity with the pathname of the downloaded image.
     */
    private Message makeReplyMessage(String dataDownloaded,
                                     Uri url,
                                     int requestCode) {
        // Get a new message via the obtain() factory method.
        Message message = Message.obtain();
        // Create a new Bundle named 'data' to handle the result.
        Bundle data = new Bundle();
        // use 'putParcelableArrayList(...)' to store the ArrayList of Entry(s) in the bundle.
        data.putString(ENTRY_ARRAY_KEY, dataDownloaded);
        // Put the requestCode into the Bundle via the REQUEST_CODE key.
        data.putInt(REQUEST_CODE, requestCode);
        // Put the url as a string into the Bundle via the FEED_URL key.
        data.putString(FEED_URL, url.toString());
        // Set a field in the Message to indicate whether the download
        // succeeded or failed.
        // sucess: Activity.RESULT_OK
        // otherwise: Activity.RESULT_CANCELED
        if(dataDownloaded != null){
            message.arg1 = Activity.RESULT_OK;
        } else {
            message.arg1 = Activity.RESULT_CANCELED;
        }
        // Set the Bundle to be the data in the message.
        message.setData(data);
        // return the message.
        return message;
    }

    /**
     * Download the YouTube AtomFeed and generate the Entry(s) that were in the Feed.
     *
     * @param feedURL The url of the YouTube feed
     * @return a List of type Entry that were in the Feed url provided.
     */
    //This was previously done by the JSONDonwloader class
    private String download(final String feedURL) {
        android.util.Log.d(TAG, "downloadAtomFeed()");
        String netData = null;

        // Download Entries
        try {
            // Make sure that this thread has not been interrupted.
            checkHaMeRThreadNotInterrupted();
            // download and process the feed from the given url.
            netData = downloadJSONData(feedURL);
            // check that thread wasn't interrupted again before returning results.
            checkHaMeRThreadNotInterrupted();
        } catch (Exception ex) {
            Log.e(TAG, "Exception downloading Atom Feed: " + ex.getMessage());
        }
        return netData;
    }

    /**
     * Helper method to check if thread has been interrupted & throw exception if it has
     *
     * @throws Exception Thrown Exception notifying that the HaMeR Thread was interrupted.
     */
    private void checkHaMeRThreadNotInterrupted() throws Exception {
        if (Thread.currentThread().isInterrupted()) {
            Log.e(TAG, "Download thread interrupted");
            throw new Exception("Thread interrupted, halting Download execution.");
        }
    }

    /**
     * Download JSON String (Network version of {@link PaperEntry} from a provided URL
     * <p>
     * There is two different classes used, because it is convienient to have independant
     * representations, even of the same data, that way one representation can be varried if
     * need be. (such as the server version changing slightly))
     *
     * @param urlString the url to grab the ATOM feed from.
     * @return the NetEntry(s) that were in the ATOM feed.
     */
    private String downloadJSONData(final String urlString) throws IOException {
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();


            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuilder buffer = new StringBuilder();
            String line = "";

            while ((line = reader.readLine()) != null) {
                buffer.append(line).append("\n");
            }

            return buffer.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
