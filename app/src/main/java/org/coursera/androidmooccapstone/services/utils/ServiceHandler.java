package org.coursera.androidmooccapstone.services.utils;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import org.coursera.androidmooccapstone.services.Downloader;

import java.lang.ref.WeakReference;

/**
 * This class inherits from Handler and uses its handleMessage() hook
 * method to forward relevant data from Messages sent from the
 * Downloader back to the MainActivity.
 */
public class ServiceHandler extends android.os.Handler {
    private final static String TAG = ServiceHandler.class.getCanonicalName();

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<ServiceResult> mResult;

    /**
     * Constructor.
     */
    public ServiceHandler(ServiceResult serviceResult) {
        mResult = new WeakReference<>(serviceResult);
    }

    /**
     * Called to reset Downloader callback instance (MainActivity)
     * after a configuration change, which will have caused the
     * garbage collector to destroy the Service object associated with
     * the mResult WeakReference.
     */
    public void onConfigurationChange(ServiceResult serviceResult) {
        mResult = new WeakReference<>(serviceResult);
    }

    /**
     * This hook method is dispatched in response to receiving the
     * results from the Downloader.
     */
    @Override
    public void handleMessage(Message message) {
        Log.d(TAG, "handleMessage() called back");

        final int requestCode = Downloader.getRequestCode(message);
        final int resultCode = Downloader.getResultCode(message);
        final Bundle data = message.getData();

        if (mResult.get() == null) {
            // Warn programmer that mResult callback reference has
            // been lost without being restored after a configuration
            // change.
            Log.w(TAG, "Configuration change handling not implemented correctly;"
                    + " lost weak reference to ServiceResult callback)");
        } else {
            // Forward result to callback implementation.
            mResult.get().onServiceResult(requestCode,
                    resultCode,
                    data
            );
        } // end of 'else'
    } // end of 'handleMessage(...)'
}
