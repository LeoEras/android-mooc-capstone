package org.coursera.androidmooccapstone.framework.orm;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import org.coursera.androidmooccapstone.framework.provider.PaperContract;

import java.util.ArrayList;

/**
 *
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class PaperEntry implements Parcelable {

    private static final String LOG_TAG = PaperEntry.class.getCanonicalName();

    public long _ID;
    public String TITLE;
    public String DATE;
    public String ABS;
    public String DOI;

    public PaperEntry(){
        this._ID = -1L;
        this.TITLE = "";
        this.DATE = "";
        this.ABS = "";
        this.DOI = "";
    }

    public PaperEntry(long _ID, String TITLE, String DATE, String ABS, String DOI) {
        this._ID = _ID;
        this.TITLE = TITLE;
        this.DATE = DATE;
        this.ABS = ABS;
        this.DOI = DOI;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaperEntry)) return false;

        PaperEntry entry = (PaperEntry) o;

        if (getTITLE() != null ? !getTITLE().equals(entry.getTITLE()) : entry.getTITLE() != null)
            return false;
        if (getDATE() != null ? !getDATE().equals(entry.getDATE()) : entry.getDATE() != null)
            return false;
        if (getABS() != null ? !getABS().equals(entry.getABS()) : entry.getABS() != null)
            return false;
        return getDOI() != null ? getDOI().equals(entry.getDOI()) : entry.getDOI() == null;

    }

    @Override
    public int hashCode() {
        int result = getTITLE() != null ? getTITLE().hashCode() : 0;
        result = 31 * result + (getDATE() != null ? getDATE().hashCode() : 0);
        result = 31 * result + (getABS() != null ? getABS().hashCode() : 0);
        result = 31 * result + (getDOI() != null ? getABS().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "_ID=" + _ID +
                ", TITLE='" + TITLE + '\'' +
                ", DATE_PUBLISHED='" + DATE + '\'' +
                ", ABSTRACT='" + ABS + '\'' +
                ", DOI='" + DOI + '\'' +
                '}';
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getABS() {
        return ABS;
    }

    public void setABS(String ABS) {
        this.ABS = ABS;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    // these are for parcelable interface
    @Override
    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public int describeContents() {
        return 0;
    }

    @Override
    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_ID);
        dest.writeString(TITLE);
        dest.writeString(DATE);
        dest.writeString(ABS);
        dest.writeString(DOI);
    }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Creator<PaperEntry> CREATOR = new Creator<PaperEntry>() {
        public PaperEntry createFromParcel(Parcel in) {
            return new PaperEntry(in);
        }

        public PaperEntry[] newArray(int size) {
            return new PaperEntry[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private PaperEntry(Parcel in) {
        _ID = in.readLong();
        TITLE = in.readString();
        DATE = in.readString();
        ABS = in.readString();
        DOI = in.readString();
    }

    /**
     * Create a ContentValues from this Entry.
     *
     * @return ContentValues that is created from the Entry object
     */
    public ContentValues getContentValues() {
        ContentValues rValue = new ContentValues();
        rValue.put(PaperContract.Entry.Cols._ID, this._ID);
        rValue.put(PaperContract.Entry.Cols.TITLE, this.TITLE);
        rValue.put(PaperContract.Entry.Cols.DATE, this.DATE);
        rValue.put(PaperContract.Entry.Cols.ABS, this.ABS);
        rValue.put(PaperContract.Entry.Cols.DOI, this.DOI);
        return rValue;
    }

    public static class CONVERTER {

        /**
         * Get all of the Papers from the passed in cursor.
         *
         * @param cursor passed in cursor
         * @return ArrayList<Papers> The set of Papers
         */
        public static ArrayList<PaperEntry> getFromCursor(
                Cursor cursor) {
            ArrayList<PaperEntry> rValue = new ArrayList<>();
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        rValue.add(getEntryFromCursor(cursor));
                    } while (cursor.moveToNext());
                }
            }
            return rValue;
        }

        /**
         * Get the first TagsData from the passed in cursor.
         *
         * @param cursor passed in cursor
         * @return TagsData object
         */
        private static PaperEntry getEntryFromCursor(Cursor cursor) {

            long _ID = cursor.getLong(cursor
                    .getColumnIndex(PaperContract.Entry.Cols._ID));

            String title = cursor.getString(cursor
                    .getColumnIndex(PaperContract.Entry.Cols.TITLE));
            String author = cursor.getString(cursor
                    .getColumnIndex(PaperContract.Entry.Cols.AUTHOR));
            String date = cursor.getString(cursor
                    .getColumnIndex(PaperContract.Entry.Cols.DATE));
            String abs = cursor.getString(cursor
                    .getColumnIndex(PaperContract.Entry.Cols.ABS));
            String doi = cursor.getString(cursor
                    .getColumnIndex(PaperContract.Entry.Cols.DOI));

            // construct the returned object
            return new PaperEntry(
                    _ID,
                    title,
                    date,
                    abs,
                    doi
            );
        }
    }
}
