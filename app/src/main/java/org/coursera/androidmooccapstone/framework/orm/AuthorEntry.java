package org.coursera.androidmooccapstone.framework.orm;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import org.coursera.androidmooccapstone.framework.provider.AuthorContract;

import java.util.ArrayList;

/**
 *
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class AuthorEntry implements Parcelable {

    private static final String LOG_TAG = AuthorEntry.class.getCanonicalName();

    public long _ID, _REFERENCE;
    public String NAME;
    public String LAST_NAME;

    public AuthorEntry(){
        this._ID = -1L;
        this.NAME = "";
        this.LAST_NAME = "";
        this._REFERENCE = -1L;
    }

    public AuthorEntry(long _ID, String NAME, String LAST_NAME, long _REFERENCE) {
        this._ID = _ID;
        this.NAME = NAME;
        this.LAST_NAME = LAST_NAME;
        this._REFERENCE = _REFERENCE;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthorEntry)) return false;

        AuthorEntry entry = (AuthorEntry) o;

        if (getNAME() != null ? !getNAME().equals(entry.getNAME()) : entry.getNAME() != null)
            return false;
        return getLAST_NAME() != null ? getLAST_NAME().equals(entry.getLAST_NAME()) : entry.getLAST_NAME() == null;

    }

    @Override
    public int hashCode() {
        int result = getNAME() != null ? getNAME().hashCode() : 0;
        result = 31 * result + (getLAST_NAME() != null ? getLAST_NAME().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "_ID=" + _ID +
                ", NAME='" + NAME + '\'' +
                ", LAST_NAME='" + LAST_NAME + '\'' +
                ", _REFERENCE='" + _REFERENCE + '\'' +
                '}';
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public long get_REFERENCE() {
        return _REFERENCE;
    }

    public void set_REFERENCE(long _REFERENCE) {
        this._REFERENCE = _REFERENCE;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    // these are for parcelable interface
    @Override
    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public int describeContents() {
        return 0;
    }

    @Override
    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_ID);
        dest.writeString(NAME);
        dest.writeString(LAST_NAME);
        dest.writeLong(_REFERENCE);
    }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Creator<AuthorEntry> CREATOR = new Creator<AuthorEntry>() {
        public AuthorEntry createFromParcel(Parcel in) {
            return new AuthorEntry(in);
        }

        public AuthorEntry[] newArray(int size) {
            return new AuthorEntry[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private AuthorEntry(Parcel in) {
        _ID = in.readLong();
        NAME = in.readString();
        LAST_NAME = in.readString();
        _REFERENCE = in.readLong();
    }

    /**
     * Create a ContentValues from this Entry.
     *
     * @return ContentValues that is created from the Entry object
     */
    public ContentValues getContentValues() {
        ContentValues rValue = new ContentValues();
        rValue.put(AuthorContract.Entry.Cols._ID, this._ID);
        rValue.put(AuthorContract.Entry.Cols.NAME, this.NAME);
        rValue.put(AuthorContract.Entry.Cols.LAST_NAME, this.LAST_NAME);
        rValue.put(AuthorContract.Entry.Cols._REFERENCE, this._REFERENCE);
        return rValue;
    }

    public static class CONVERTER {

        /**
         * Get all of the Authors from the passed in cursor.
         *
         * @param cursor passed in cursor
         * @return ArrayList<Authors> The set of Authors
         */
        public static ArrayList<AuthorEntry> getFromCursor(
                Cursor cursor) {
            ArrayList<AuthorEntry> rValue = new ArrayList<>();
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        rValue.add(getEntryFromCursor(cursor));
                    } while (cursor.moveToNext());
                }
            }
            return rValue;
        }

        /**
         * Get the first TagsData from the passed in cursor.
         *
         * @param cursor passed in cursor
         * @return TagsData object
         */
        private static AuthorEntry getEntryFromCursor(Cursor cursor) {

            long _ID = cursor.getLong(cursor
                    .getColumnIndex(AuthorContract.Entry.Cols._ID));

            String name = cursor.getString(cursor
                    .getColumnIndex(AuthorContract.Entry.Cols.NAME));
            String last_name = cursor.getString(cursor
                    .getColumnIndex(AuthorContract.Entry.Cols.LAST_NAME));
            long _REFERENCE = cursor.getLong(cursor
                    .getColumnIndex(AuthorContract.Entry.Cols._REFERENCE));

            // construct the returned object
            return new AuthorEntry(
                    _ID,
                    name,
                    last_name,
                    _REFERENCE
            );
        }
    }
}
