package org.coursera.androidmooccapstone.framework.interfaces;

import androidx.annotation.NonNull;

import org.coursera.androidmooccapstone.framework.orm.AuthorEntry;
import org.coursera.androidmooccapstone.framework.orm.PaperEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Interface to allow communication between RecyclerView Fragment and Activity.
 */

public interface UpdateEntriesInterface {

    /**
     * Displays the specified list of entries in the recycler view.
     * @param entries List of entries to display.
     */
    void updatePaperEntries(List<PaperEntry> entries);

    /**
     * Displays the specified list of entries in the recycler view.
     * @param entries List of entries to display.
     */
    void updateAuthorEntries(List<AuthorEntry> entries);


    /**
     * Returns the list of currently displayed entry(s).
     * @return An array list of entries (possibly empty).
     */
    @NonNull
    ArrayList<PaperEntry> getPaperEntries();
    ArrayList<AuthorEntry> getAuthorEntries();
}