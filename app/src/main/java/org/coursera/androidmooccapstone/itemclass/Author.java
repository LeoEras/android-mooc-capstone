package org.coursera.androidmooccapstone.itemclass;

import androidx.annotation.NonNull;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Author {
    private long id, reference;
    private String name, lastName;

    public Author(long id, String name, String lastName, long reference){
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public long getId(){ return id; }

    public long getReference() {
        return reference;
    }

    public static Author[] getAuthorsByReference(ArrayList<Author> authors, long reference){
        ArrayList<Author> subset = new ArrayList<>();
        for(Author author : authors){
            if(author.getReference() == reference){
                subset.add(author);
            }
        }
        Author[] authorResponse = new Author[subset.size()];
        authorResponse = subset.toArray(authorResponse);
        return authorResponse;
    }

    @NonNull
    @Override
    public String toString() {
        return getName() + " " + getLastName();
    }
}
