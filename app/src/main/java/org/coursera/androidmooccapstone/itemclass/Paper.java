package org.coursera.androidmooccapstone.itemclass;

import java.util.ArrayList;

/*
* This is a paper, a scientific paper in journals.
* */
public class Paper {
    private final long id;
    private final String title, date, abs, doi; //abstract is a reserved word.
    private final Author[] authors;

    public Paper(long id, String title, Author[] authors, String date, String abs, String doi){
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.date = date;
        this.abs = abs;
        this.doi = doi;
    }

    public long getId(){ return id; }

    public static Paper findById(long id, ArrayList<Paper> listOfPapers){
        for(Paper item : listOfPapers){
            if(item.getId() == id){
                return item;
            }
        }
        return null;
    }

    public String getDate() {
        return date;
    }

    public String getAbs() {
        return abs;
    }

    public String getTitle() {
        return title;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public String getDoi() {
        return doi;
    }
}
