package org.coursera.androidmooccapstone.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.coursera.androidmooccapstone.framework.provider.AuthorContract;
import org.coursera.androidmooccapstone.framework.provider.PaperContract;
import org.coursera.androidmooccapstone.framework.provider.PaperDBAdapter;

import static android.content.UriMatcher.NO_MATCH;
import static org.coursera.androidmooccapstone.framework.provider.PaperContract.Entry.CONTENT_URI;
import static org.coursera.androidmooccapstone.framework.provider.PaperContract.Entry.TABLE_NAME;

public class PaperProvider extends ContentProvider {
    private final static String TAG = PaperProvider.class.getCanonicalName();
    // The Adapter to the Database.
    private PaperDBAdapter mDB;
    private static final int PAPER_ALL_ENTRY_ROWS = PaperContract.Entry.PATH_TOKEN;
    private static final int PAPER_SINGLE_ENTRY_ROW = PaperContract.Entry.PATH_FOR_ID_TOKEN;
    private static final int AUTHOR_ALL_ENTRY_ROWS = AuthorContract.Entry.PATH_TOKEN;
    private static final int AUTHOR_SINGLE_ENTRY_ROW = AuthorContract.Entry.PATH_FOR_ID_TOKEN;
    final UriMatcher mPaperUriMatcher = PaperContract.URI_MATCHER;
    final UriMatcher mAuthorUriMatcher = AuthorContract.URI_MATCHER;

    @Override
    public boolean onCreate() {
        mDB = new PaperDBAdapter(getContext(), false);
        mDB.open();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        Log.d(TAG, "query(...) Uri: " + uri);

        // create copies of selection and sortOrder.
        String modifiedSelection = selection;
        String updatedSortOrder = sortOrder;
        Cursor cursor = null;

        UriMatcher mUriMatcher = initializeUriMatcher(uri);

        switch(mUriMatcher.match(uri)) {

            // if SINGLE_ROW & selection is null, then set modifiedSelection to:
            // "_ID = " + uri .getLastPathSegment()
            // else make it:
            // modifiedSelection += " AND " + <the same as above>
            case PAPER_SINGLE_ENTRY_ROW:
                if(selection == null){
                    modifiedSelection = "_ID = " + uri.getLastPathSegment();
                } else {
                    modifiedSelection += " AND " + "_ID = " + uri.getLastPathSegment();
                }
                cursor = query(uri, TABLE_NAME, projection, modifiedSelection, selectionArgs, updatedSortOrder);
                break;
            // if matcher matches ALL_ROWS,
            // if sortOrder is empty, then set new copy of sortOrder to "_ID ASC"
            // then call private query method with appropriate parameters(including new copy of
            // sortOrder).
            case PAPER_ALL_ENTRY_ROWS:
                if(sortOrder == null){
                    updatedSortOrder = "_ID ASC";
                }
                cursor = query(uri, TABLE_NAME, projection, modifiedSelection, selectionArgs, updatedSortOrder);
                break;
            // if matcher matches UriMatcher.NO_MATCH or default,
            // then throw new IllegalArgumentException("Invalid URI")
            case AUTHOR_SINGLE_ENTRY_ROW:
                if(selection == null){
                    modifiedSelection = "_ID = " + uri.getLastPathSegment();
                } else {
                    modifiedSelection += " AND " + "_ID = " + uri.getLastPathSegment();
                }
                cursor = query(uri, AuthorContract.Entry.TABLE_NAME, projection, modifiedSelection, selectionArgs, updatedSortOrder);
                break;
            // if matcher matches ALL_ROWS,
            // if sortOrder is empty, then set new copy of sortOrder to "_ID ASC"
            // then call private query method with appropriate parameters(including new copy of
            // sortOrder).
            case AUTHOR_ALL_ENTRY_ROWS:
                if(sortOrder == null){
                    updatedSortOrder = "_ID ASC";
                }
                cursor = query(uri, AuthorContract.Entry.TABLE_NAME, projection, modifiedSelection, selectionArgs, updatedSortOrder);
                break;
            case NO_MATCH:
                throw new IllegalArgumentException("Invalid URI");
        }
        return cursor;
    }

    /*
    * Quite fuggly but functional repair to issue of having more than one UriMatcher
    * */
    private UriMatcher initializeUriMatcher(Uri uri) {
        UriMatcher mUriMatcher = mPaperUriMatcher;
        if(mPaperUriMatcher.match(uri) == PAPER_SINGLE_ENTRY_ROW || mPaperUriMatcher.match(uri) == PAPER_ALL_ENTRY_ROWS ){
            mUriMatcher = mPaperUriMatcher;
        } else if (mAuthorUriMatcher.match(uri) == AUTHOR_SINGLE_ENTRY_ROW || mAuthorUriMatcher.match(uri) == AUTHOR_ALL_ENTRY_ROWS){
            mUriMatcher = mAuthorUriMatcher;
        }
        return mUriMatcher;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        String result;
        UriMatcher mUriMatcher = initializeUriMatcher(uri);
        switch (mUriMatcher.match(uri)){
            case PAPER_SINGLE_ENTRY_ROW:
                result = PaperContract.Entry.CONTENT_ITEM_TYPE;
                break;
            case PAPER_ALL_ENTRY_ROWS:
                result = PaperContract.Entry.CONTENT_TYPE_DIR;
                break;
            case AUTHOR_SINGLE_ENTRY_ROW:
                result = AuthorContract.Entry.CONTENT_ITEM_TYPE;
                break;
            case AUTHOR_ALL_ENTRY_ROWS:
                result = AuthorContract.Entry.CONTENT_TYPE_DIR;
                break;
            default:
                throw new UnsupportedOperationException("URI: " + uri + " is not supported.");
        }
        return result;
    }

    /**
     * Private query that does the actual query based on the table.
     * <p>
     * This method makes use of SQLiteQueryBuilder to build a simple query.
     */
    synchronized private Cursor query(final Uri uri, final String tableName,
                                      final String[] projection, final String selection,
                                      final String[] selectionArgs, final String sortOrder) {
        // Make a new SQLiteQueryBuilder object.
        SQLiteQueryBuilder newQuery = new SQLiteQueryBuilder();

        // set the table(s) to be queried upon.
        newQuery.setTables(tableName);

        // return the builder.query(....) result, after passing the appropriate values.
        return newQuery.query(mDB.getDB(), projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues assignedValues) {
        UriMatcher mUriMatcher = initializeUriMatcher(uri);
        switch (mUriMatcher.match(uri)) {
            // if the match equals a PATH TOKEN for ALL ROWS of a table in the FeedContract
            case PAPER_ALL_ENTRY_ROWS:
                // create a ContentValues, then remove the '_ID' value from it.
                ContentValues values = new ContentValues(assignedValues);
                values.remove(PaperContract.Entry.Cols._ID);
                // insert the ContentValues into the database adapater instance and store the
                // returned rowID.
                long rowID = mDB.insert(TABLE_NAME, values);
                // if returned rowID is < 0, then return null.
                if (rowID < 0) {
                    return null;
                }
                // Use ContentUris.withAppendedId(Uri, long) to create a new Uri.
                // use the appropriate table's CONTENT_URI and the rowID.
                Uri contentUri = ContentUris.withAppendedId(CONTENT_URI, rowID);
                // Call notifyChanges on this, with the new Uri, and a null content observer
                // This calls notifyChange(Uri, ContentObserver) in setup Activities.
                notifyChanges(contentUri, null);
                // Return the notification Uri
                return contentUri;
            // if the match equals a PATH TOKEN for ALL ROWS of a table in the FeedContract
            case AUTHOR_ALL_ENTRY_ROWS:
                // create a ContentValues, then remove the '_ID' value from it.
                values = new ContentValues(assignedValues);
                values.remove(AuthorContract.Entry.Cols._ID);
                // insert the ContentValues into the database adapater instance and store the
                // returned rowID.
                rowID = mDB.insert(AuthorContract.Entry.TABLE_NAME, values);
                // if returned rowID is < 0, then return null.
                if (rowID < 0) {
                    return null;
                }
                // Use ContentUris.withAppendedId(Uri, long) to create a new Uri.
                // use the appropriate table's CONTENT_URI and the rowID.
                contentUri = ContentUris.withAppendedId(AuthorContract.Entry.CONTENT_URI, rowID);
                // Call notifyChanges on this, with the new Uri, and a null content observer
                // This calls notifyChange(Uri, ContentObserver) in setup Activities.
                notifyChanges(contentUri, null);
                // Return the notification Uri
                return contentUri;
            // in both cases: 1) a Single Row URI was passed, or 2) default case: throw a new
            //  IllegalArgumentException("Unsupported URI, unable to insert into specific row: "
            //        + uri);
            default:
                throw new IllegalArgumentException("Unsupported URI, unable to insert into specific row: " + uri);
        }

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        // copy of whereClause to modify if needed.
        String modifiedWhereClause = selection;
        String TABLE_NAME = null;

        // switch on mUriMatcher.match
        UriMatcher mUriMatcher = initializeUriMatcher(uri);
        switch(mUriMatcher.match(uri)) {
            case PAPER_SINGLE_ENTRY_ROW:
                if(selection == null){
                    modifiedWhereClause = "_ID = " + uri.getLastPathSegment();
                } else {
                    modifiedWhereClause += " AND " + "_ID = " + uri.getLastPathSegment();
                }
                TABLE_NAME = PaperContract.Entry.TABLE_NAME;
                break;
            case AUTHOR_SINGLE_ENTRY_ROW:
                if(selection == null){
                    modifiedWhereClause = "_ID = " + uri.getLastPathSegment();
                } else {
                    modifiedWhereClause += " AND " + "_ID = " + uri.getLastPathSegment();
                }
                TABLE_NAME = AuthorContract.Entry.TABLE_NAME;
                break;
            case PAPER_ALL_ENTRY_ROWS:
                TABLE_NAME = PaperContract.Entry.TABLE_NAME;
                break;
            case AUTHOR_ALL_ENTRY_ROWS:
                TABLE_NAME = AuthorContract.Entry.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        return deleteAndNotify(uri, TABLE_NAME, modifiedWhereClause, selectionArgs);
    }

    /*
     * Private method to both attempt the delete command, and then to notify of
     * the changes
     */
    private int deleteAndNotify(final Uri uri, final String tableName,
                                final String whereClause, final String[] whereArgs) {
        // call delete on DBAdapter instance, and store the int number or rows deleted.
        int count = mDB.delete(tableName, whereClause, whereArgs);

        // if count > 0, then call notifyChange on the Uri provided. (with null observer)
        // then return the count.
        if(count > 0){
            notifyChanges(uri, null);
        }
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        // WhereClause copy for use in modifing the whereClause
        String modifedWhereClause = selection;

        // remove "_ID" from the content values.
        //values.remove("_ID");
        values.remove(PaperContract.Entry.Cols._ID);
        values.remove(AuthorContract.Entry.Cols._ID);

        // switch based on the uri provided using mUriMatcher
        int value;

        UriMatcher mUriMatcher = initializeUriMatcher(uri);
        switch (mUriMatcher.match(uri)) {

            // if uri matches single row of table, modify whereClause appropriately:
            // If whereClause is null, then set modifiedWhereClause to _ID = last path segement
            // If whereClause is nonNull, then append " AND " and then the same as above.
            case PAPER_SINGLE_ENTRY_ROW:
                if(selection == null){
                    modifedWhereClause = "_ID = " + uri.getLastPathSegment();
                } else {
                    modifedWhereClause += " AND " + "_ID = " + uri.getLastPathSegment();
                }

                return updateAndNotify(uri, TABLE_NAME, values, modifedWhereClause, selectionArgs);
            // if whereClause was modified, or if Uri matches all rows of a table, then call
            // updateAndNotify(...) with appropriate parameters.
            case PAPER_ALL_ENTRY_ROWS:
                return updateAndNotify(uri, TABLE_NAME, values, modifedWhereClause, selectionArgs);
            case AUTHOR_SINGLE_ENTRY_ROW:
                if(selection == null){
                    modifedWhereClause = "_ID = " + uri.getLastPathSegment();
                } else {
                    modifedWhereClause += " AND " + "_ID = " + uri.getLastPathSegment();
                }

                return updateAndNotify(uri, AuthorContract.Entry.TABLE_NAME, values, modifedWhereClause, selectionArgs);
            // if whereClause was modified, or if Uri matches all rows of a table, then call
            // updateAndNotify(...) with appropriate parameters.
            case AUTHOR_ALL_ENTRY_ROWS:
                return updateAndNotify(uri, AuthorContract.Entry.TABLE_NAME, values, modifedWhereClause, selectionArgs);
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    /*
     * private update function that updates based on parameters, then notifies
     * change
     */
    private int updateAndNotify(@NonNull final Uri uri, final String tableName,
                                final ContentValues values, final String whereClause,
                                final String[] whereArgs) {
        // call update(...) on the DBAdapter instance variable, and store the count of rows updated.
        int count = mDB.update(tableName, values, whereClause, whereArgs);

        // if count > 0 then call notifyChanges(...) on the Uri (null observer), and return the
        // count.
        if(count > 0){
            notifyChanges(uri, null);
        }
        return count;
    }

    private void notifyChanges(Uri uri, ContentObserver contentObserver) {
        Context context = getContext();
        if (context == null) {
            return;
        }
        ContentResolver resolver = context.getContentResolver();
        if (resolver == null) {
            return;
        }
        resolver.notifyChange(uri, null);
    }
}
