package org.coursera.androidmooccapstone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.coursera.androidmooccapstone.adapters.PaperAdapter;
import org.coursera.androidmooccapstone.framework.orm.AuthorEntry;
import org.coursera.androidmooccapstone.framework.orm.PaperEntry;
import org.coursera.androidmooccapstone.itemclass.Author;
import org.coursera.androidmooccapstone.itemclass.Paper;
import org.coursera.androidmooccapstone.services.Downloader;
import org.coursera.androidmooccapstone.services.utils.ServiceHandler;
import org.coursera.androidmooccapstone.services.utils.ServiceResult;
import org.coursera.androidmooccapstone.utils.ProviderUtils;
import org.coursera.androidmooccapstone.utils.SwipeToDeleteCallback;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static org.coursera.androidmooccapstone.utils.Utils.ArrayToString;
import static org.coursera.androidmooccapstone.utils.Utils.INTENT_TAP;

public class MainActivity extends AppCompatActivity implements ServiceResult{
    private final static String TAG = MainActivity.class.getCanonicalName();
    private RecyclerView recyclerView;
    private TextView title, author, date, abs;
    private final ArrayList<Paper> paperList = new ArrayList<>();
    private Button openInBrowser, eraseData;

    /**
     * variable we use within the Main Activity to 'flag' what kind of request is being returned
     * to us. This is less useful when an Activity only requests 1 thing. However, as soon as an
     * Activity starts requesting more than one thing, it becomes crucial to know what request
     * type is being returned. Therefore, it is best to get into the practice of always using
     * request variables like this.
     */
    private final static int REQUEST_PAPER_ENTRIES = 1;

    /**
     * Stores an instance of ServiceResultHandler.
     */
    private Handler mServiceResultHandler = null;

    private final static String PAPERS_URL = "https://androidmooccapstone.getsandbox.com/papers";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        mServiceResultHandler = new ServiceHandler((ServiceResult) this);

        final String msg = getResources().getString(R.string.fetching, PAPERS_URL);
        FloatingActionButton fetchData = findViewById(R.id.reload);
        fetchData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDownload(Uri.parse(PAPERS_URL));

                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });

        //Configuring LocalBroadcastManager and filters
        configureBroadcastManager();
        //Initiating the TextViews
        initiateViews();
        //Getting the recyclerView with the adapter ready
        setRecyclerView();
        //Enable swipe to delete
        enableSwipeToDelete();
        //Example RecyclerViews item for testing only
        //initExamplesForTesting();
        //Loading data, if any from db, done as a separate thread.
        final ArrayList<PaperEntry> paperEntries = new ArrayList<>();
        final ArrayList<AuthorEntry> authorEntries = new ArrayList<>();
        final ArrayList<Paper> papersFromDB = new ArrayList<>();
        final ArrayList<Author> authorsFromDB = new ArrayList<>();
        Thread thread = new Thread() {
            @Override
            public void run() {
                paperEntries.addAll(ProviderUtils.loadPapersFromProvider(MainActivity.this));
                authorEntries.addAll(ProviderUtils.loadAuthorsFromProvider(MainActivity.this));

                for(AuthorEntry authorEntry : authorEntries){
                    authorsFromDB.add(new Author(authorEntry.get_ID(), authorEntry.getNAME(), authorEntry.getLAST_NAME(), authorEntry.get_REFERENCE()));
                }

                for(PaperEntry paperEntry : paperEntries){
                    //Log.d(TAG, "" + paperEntry.get_ID());
                    Author[] authors = Author.getAuthorsByReference(authorsFromDB, paperEntry.get_ID());
                    //Log.d(TAG, authors[0].getName());
                    papersFromDB.add(new Paper(paperEntry.get_ID(), paperEntry.getTITLE(), authors, paperEntry.getDATE(), paperEntry.getABS(), paperEntry.getDOI()));
                }
                paperList.clear();
                paperList.addAll(papersFromDB);
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        };
        thread.start();
    }

    /**
     * This method is called after {@link #onStart} when the activity is being
     * re-initialized from a previously saved state, given here in
     * <var>savedInstanceState</var>.
     *
     * @param savedInstanceState the data most recently supplied in {@link
     *                           #onSaveInstanceState}.
     */
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


    /**
     * Hook method called when a configuration change is about to occur.
     *
     * @param outState the Bundle which stores the state
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        //ArrayList<PaperEntry> entries = updateEntriesInterface.getPaperEntries();
        //outState.putParcelableArrayList("Entries", entries);
        super.onSaveInstanceState(outState);
    }

    /**
     * Called when a launched Service sends back results from
     * computations it runs, giving the requestCode it was started
     * with, the resultCode it returned, and any additional data from
     * it.  The resultCode will be RESULT_CANCELED if the Service
     * explicitly returned that.
     *
     * @param requestCode the request code
     * @param resultCode  the result code
     * @param data        the Bundle containing the results of the Service call.
     */
    @Override
    public void onServiceResult(int requestCode, int resultCode, Bundle data) {
        // check if resultCode = Activity.RESULT_CANCELED, if it does, then call
        // handleDownloadFailure and return;
        if(resultCode == RESULT_CANCELED){
            handleDownloadFailure(data);
            return;
        }

        // Otherwise **resultCode == Activity.RESULT_OK**
        // Handle a successful download.
        // Log to both the on-screen & logcat logs the requestUri from the data.
        if(resultCode == RESULT_OK){
            // Build the dataDownloaded as entries.
            // Log to the on-screen and logcat logs the number of entries downloaded.
            final String dataDownloaded = Downloader.getDataDownloaded(data);
            Type listType = new TypeToken<ArrayList<Paper>>() {}.getType();
            Gson gson = new Gson();
            ArrayList<Paper> papers;
            papers = gson.fromJson(dataDownloaded, listType);
            final ArrayList<PaperEntry> paperEntries = new ArrayList<>();
            final ArrayList<AuthorEntry> authorEntries = new ArrayList<>();
            for (Paper paper : papers){
                paperEntries.add(new PaperEntry(paper.getId(), paper.getTitle(), paper.getDate(), paper.getAbs(), paper.getDoi()));
                for (Author author : paper.getAuthors()){
                    authorEntries.add(new AuthorEntry(author.getId(), author.getName(), author.getLastName(), paper.getId()));
                }
            }

            Thread thread = new Thread() {
                @Override
                public void run() {
                    ProviderUtils.deletePaperProviderContents(MainActivity.this);
                    ProviderUtils.deleteAuthorProviderContents(MainActivity.this);
                    ProviderUtils.addPaperEntriesToProvider(MainActivity.this, paperEntries);
                    ProviderUtils.addAuthorEntriesToProvider(MainActivity.this, authorEntries);
                }
            };

            paperList.clear();
            paperList.addAll(papers);
            recyclerView.getAdapter().notifyDataSetChanged();
            thread.start();
        }
    }

    /**
     * Handle failure to download an image.
     *
     * @param data the Bundle containing the download attempt's information.
     */
    private void handleDownloadFailure(Bundle data) {
        // Extract the URL from the message.
        final Uri url = Downloader.getRequestUri(data);

        Toast.makeText(this,
                "Feed at :\n"
                        + url.toString()
                        + "\n failed to download!",
                Toast.LENGTH_LONG
        ).show();
    }

    private void configureBroadcastManager() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(INTENT_TAP);
        LocalBroadcastManager.getInstance(this).registerReceiver(recyclerViewItemListener, filter);
    }

    private void initiateViews() {
        title = findViewById(R.id.title);
        author = findViewById(R.id.auth);
        date = findViewById(R.id.date);
        abs = findViewById(R.id.abs);
        abs.setMovementMethod(new ScrollingMovementMethod());
        openInBrowser = findViewById(R.id.open_in_browser);
        openInBrowser.setEnabled(false);
        eraseData = findViewById(R.id.erase_data);

        eraseData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInBrowser.setEnabled(false);
                if(!paperList.isEmpty()){
                    Toast.makeText(getApplicationContext(), R.string.erasing, Toast.LENGTH_SHORT).show();
                    ProviderUtils.deletePaperProviderContents(MainActivity.this);
                    ProviderUtils.deleteAuthorProviderContents(MainActivity.this);
                    paperList.clear();
                    recyclerView.getAdapter().notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_data, Toast.LENGTH_SHORT).show();
                }
                cleanTopView();
            }
        });
    }

    private void cleanTopView() {
        title.setText(R.string.big_title);
        author.setText(R.string.big_auth);
        date.setText(R.string.big_date);
        abs.setText(R.string.big_abs);
    }

    private void setRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        final PaperAdapter paperAdapter = new PaperAdapter(this, paperList); //paperList is the arraylist
        recyclerView.setAdapter(paperAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * Start a download.
     */
    private void startDownload(Uri url) {
        // Create an intent to download the paper data.
        Intent downloadIntent = Downloader.makeIntent(this, REQUEST_PAPER_ENTRIES, url, mServiceResultHandler);
        Log.d(TAG,"starting the Downloader intent service for "+ url.toString());
        // call startService on that Intent.
        startService(downloadIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(recyclerViewItemListener);
        //paperDBAdapter.close();
    }

    private final BroadcastReceiver recyclerViewItemListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action){ //Could be replaced by if here, but with this we can expand actions.
                case INTENT_TAP:
                    final long id = intent.getLongExtra("id", -1);
                    if(id >= 0){ //Made mistake with position > 0... :C Should return to CS101
                        final Paper paper = Paper.findById(id, paperList);
                        title.setText(paper.getTitle());
                        title.setTypeface(null, Typeface.BOLD);
                        author.setText(ArrayToString(paper.getAuthors()));
                        date.setText(paper.getDate());
                        abs.setText(paper.getAbs());
                        openInBrowser.setEnabled(true);
                        enableMoreInfoAction(paper.getDoi());
                        invalidateOptionsMenu();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void enableMoreInfoAction(String url) {
        final Intent launchBrowser = new Intent(Intent.ACTION_VIEW);
        launchBrowser.setData(Uri.parse("https://doi.org/" + url));

        openInBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(launchBrowser);
            }
        });
    }

    private void enableSwipeToDelete() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {


                final int position = viewHolder.getAdapterPosition();
                final PaperAdapter mAdapter = (PaperAdapter) recyclerView.getAdapter();
                final Paper paper = mAdapter.getData().get(position);

                mAdapter.removeItem(position);

                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        ProviderUtils.deletePaper(MainActivity.this, Long.toString(paper.getId()));
                        //Kind of a cascade delete here!
                        ProviderUtils.deleteAuthor(MainActivity.this, Long.toString(paper.getId()));
                    }
                };
                thread.start();
            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }
}