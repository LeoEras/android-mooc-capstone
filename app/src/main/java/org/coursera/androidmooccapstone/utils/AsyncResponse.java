package org.coursera.androidmooccapstone.utils;

public interface AsyncResponse {
    void processFinish(String output);
}