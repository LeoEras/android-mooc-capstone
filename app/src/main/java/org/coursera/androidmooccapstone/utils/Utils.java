package org.coursera.androidmooccapstone.utils;

import org.coursera.androidmooccapstone.itemclass.Author;

public final class Utils {
    public final static String INTENT_TAP = "android.intent.action.INTENT_TAP";
    public final static String INTENT_TEST_SHOW_PAPER = "android.intent.action.INTENT_TEST_SHOW_PAPER";
    private Utils(){}

    public static String ArrayToString(final Author[] authors){
        StringBuilder prettyAuthors = new StringBuilder();
        prettyAuthors.append("");
        final int size = authors.length;
        for(int i = 0; i < size; i++){
            prettyAuthors.append(authors[i].toString());
            if(i < size - 1){
                prettyAuthors.append(", ");
            }
        }
        return prettyAuthors.toString();
    }
}
