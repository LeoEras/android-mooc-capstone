package org.coursera.androidmooccapstone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import org.coursera.androidmooccapstone.R;
import org.coursera.androidmooccapstone.itemclass.Paper;

import java.util.ArrayList;

import static org.coursera.androidmooccapstone.utils.Utils.ArrayToString;
import static org.coursera.androidmooccapstone.utils.Utils.INTENT_TAP;

public class PaperAdapter extends RecyclerView.Adapter<PaperAdapter.ViewHolder>{
    private ArrayList<Paper> listOfPapers;
    private Context context;

    public PaperAdapter(Context context, ArrayList<Paper> paperItems){
        this.context = context;
        this.listOfPapers = paperItems;
    }

    @NonNull
    @Override
    public PaperAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PaperAdapter.ViewHolder holder, final int position) {
        String authors = "";
        holder.paper = listOfPapers.get(position);
        holder.title.setText(holder.paper.getTitle());
        authors = ArrayToString(holder.paper.getAuthors());
        holder.author.setText(authors);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent onTap = new Intent(INTENT_TAP);
                onTap.putExtra("id", holder.paper.getId()); //We need the ith PaperItem to get its data
                LocalBroadcastManager.getInstance(context).sendBroadcast(onTap);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listOfPapers.size();
    }

    public void removeItem(int position) {
        listOfPapers.remove(position);
        notifyItemRemoved(position);
    }

    public ArrayList<Paper> getData() {
        return listOfPapers;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title, author;
        private Paper paper;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.entry_row_title);
            title.setTypeface(null, Typeface.BOLD);
            author = itemView.findViewById(R.id.entry_row_author);
        }
    }
}
