package org.coursera.androidmooccapstone.provider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

import org.coursera.androidmooccapstone.framework.orm.AuthorEntry;
import org.coursera.androidmooccapstone.framework.orm.PaperEntry;
import org.coursera.androidmooccapstone.framework.provider.AuthorContract;
import org.coursera.androidmooccapstone.framework.provider.PaperContract;
import org.coursera.androidmooccapstone.framework.provider.PaperDBAdapter;

import java.util.ArrayList;
import java.util.List;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotEquals;

/**
 * https://developer.android.com/training/testing/integration-testing/content-provider-testing.html
 * https://developer.android.com/training/testing/start/index.html#run-instrumented-tests
 */
public class PaperProvider_AndroidTest extends ProviderTestCase2<PaperProvider> {

    // The class reference of what ContentProvider we are testing
    static final private Class<PaperProvider> providerClassName =
            PaperProvider.class;

    // The Provider's Authority
    static final private String paperAuthority = PaperContract.AUTHORITY;
    // URI for Item(s)
    private static final Uri mPaperEntryUri = PaperContract.Entry.CONTENT_URI;
    // URI for Item(s)
    private static final Uri mAuthorEntryUri = AuthorContract.Entry.CONTENT_URI;

    // ContentProvider to test
    private PaperProvider paperProvider;
    // Mock ContentResolver
    private ContentResolver mPaperMockResolver, mAuthorMockResolver;

    static final public long PAPER_TEST_ID = 123456L;
    static final public String TEST_TITLE = "Lets party like its 1999";
    static final public String TEST_DATE = "20-02-2020";
    static final public String TEST_ABS = "An interesting abstract paragraph";
    static final public String TEST_DOI = "example_doi/202913.3021";

    static final public long AUTHOR_TEST_ID = 123457L;
    static final public String TEST_NAME = "Leonardo";
    static final public String TEST_LASTNAME = "Eras";

    static public PaperEntry newPaperEntry() {
        return new PaperEntry(PAPER_TEST_ID, TEST_TITLE, TEST_DATE, TEST_ABS, TEST_DOI);
    }

    static public PaperEntry newPaperEntry2() {
        return new PaperEntry(PAPER_TEST_ID, "A wonderful paper", TEST_DATE, TEST_ABS, TEST_DOI);
    }

    static public AuthorEntry newAuthorEntry() {
        return new AuthorEntry(AUTHOR_TEST_ID , TEST_NAME, TEST_LASTNAME, PAPER_TEST_ID);
    }

    static public AuthorEntry newAuthorEntry2() {
        return new AuthorEntry(AUTHOR_TEST_ID , "Some amazing name here", TEST_LASTNAME, PAPER_TEST_ID);
    }

    private final PaperEntry testPaperEntry = newPaperEntry();
    private final PaperEntry testPaperEntry2 = newPaperEntry2();
    private final AuthorEntry testAuthorEntry = newAuthorEntry();
    private final AuthorEntry testAuthorEntry2 = newAuthorEntry2();

    private PaperDBAdapter mPaperDBAdapter;

    /**
     * Constructor.
     */
    public PaperProvider_AndroidTest() {
        super(providerClassName, paperAuthority);
    }

    /*
     * Setup that will run before every test method
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setContext(getApplicationContext());

        // get Mock Context and Mock ContentResolver.
        Context mMockContext = getMockContext();
        mPaperMockResolver = mMockContext.getContentResolver();
        mAuthorMockResolver = mMockContext.getContentResolver();

        // assign the authority for the provider we will create next.
        ProviderInfo providerPaperInfo = new ProviderInfo();
        ProviderInfo providerAuthorInfo = new ProviderInfo();
        providerPaperInfo.authority = PaperContract.AUTHORITY;
        providerAuthorInfo.authority = AuthorContract.AUTHORITY;
        // create provider
        paperProvider = new PaperProvider(); //There's only one
        // attach info to the provider
        paperProvider.attachInfo(mMockContext, providerPaperInfo);
        paperProvider.attachInfo(mMockContext, providerAuthorInfo);

        // verify that mRssContentProvider exists.
        assertNotNull(paperProvider);

        // add our provider to the list of providers in the mock ContentResolver
        ((MockContentResolver) mPaperMockResolver).addProvider(PaperContract.AUTHORITY, paperProvider);
    }

    /**
     * Method that will run after every test
     */
    @Override
    public void tearDown() {
        paperProvider.shutdown();
    }

    /**
     * Get the MockContentResolver object, can be altered in future if need be.
     *
     * @return the MockContentResolver object
     */
    @Override
    @SuppressWarnings("EmptyMethod")
    public MockContentResolver getMockContentResolver() {
        return super.getMockContentResolver();
    }

    /**
     * Test Insert of single PaperEntry and AuthorEntry into ContentProvider.
     */
    public void testInsertEntry() {
        // get ContentValues from mPaperItem
        ContentValues values = testPaperEntry.getContentValues();
        // use mock ContentResolver to insert mItem's ContentValues
        Uri resultingUri = mPaperMockResolver.insert(mPaperEntryUri, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        Cursor cursor = mPaperMockResolver.query(mPaperEntryUri,
                PaperContract.Entry.ALL_COLUMN_NAMES,
                null,
                null,
                null
        );

        ArrayList<PaperEntry> entries = PaperEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(entries.get(0), testPaperEntry);


        // get ContentValues from mPaperItem
        values = testAuthorEntry.getContentValues();
        // use mock ContentResolver to insert mItem's ContentValues
        resultingUri = mAuthorMockResolver.insert(mAuthorEntryUri, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        cursor = mAuthorMockResolver.query(mAuthorEntryUri,
                AuthorContract.Entry.ALL_COLUMN_NAMES,
                null,
                null,
                null
        );

        ArrayList<AuthorEntry> entries2 = AuthorEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(entries2.get(0), testAuthorEntry);
    }

    /**
     * Testing for empty both for Paper and for Author
     */
    public void testQueryEmpty() {
        // query the mock ContentResolver on the Item URI, return all rows.
        Cursor cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        // verify results
        assertNotNull(cursor);
        // results should be 0 IF THE APP WASN'T USED BEFORE because setUp doesn't insert anything into the provider to start
        assertThat(cursor.getCount(), is(0));

        // query the mock ContentResolver on the Item URI, return all rows.
        cursor = mPaperMockResolver.query(mAuthorEntryUri, null, null, null, null);
        // verify results
        assertNotNull(cursor);
        // results should be 0 IF THE APP WASN'T USED BEFORE because setUp doesn't insert anything into the provider to start
        assertThat(cursor.getCount(), is(0));
    }

    /**
     * Very basic query test. Inserting a PaperEntry and AuthorEntry on the db, then checking. First
     * for Papers and then for Authors
     */
    public void testQueryNonEmpty() {
        // get ContentValues and insert it into Provider via ContentResolver
        ContentValues values = testPaperEntry.getContentValues();
        Uri resultUri = mPaperMockResolver.insert(mPaperEntryUri, values);

        // query the mock ContentResolver on the Item URI, return all rows.
        Cursor cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        // verify cursor non-null
        assertNotNull(cursor);
        // verify results size = 1
        assertThat(cursor.getCount(), is(1));
        // get List<Entry> from the cursor.
        ArrayList<PaperEntry> paperEntries = PaperEntry.CONVERTER.getFromCursor(cursor);
        // make sure 1 and only 1 entry was generated from the cursor
        assertThat(paperEntries.size(), is(1));
        // get the inserted Entry
        PaperEntry entry = paperEntries.get(0);
        // check that returned Entry is non-null
        assertNotNull(entry);
        // verify that the returned Entry matches (other than _ID) the inserted Entry.
        assertEquals(entry, testPaperEntry);

        //Check if query with uri with ID works
        cursor = mPaperMockResolver.query(resultUri, null, null, null, null);
        // verify cursor non-null
        assertNotNull(cursor);
        // verify results size = 1
        assertThat(cursor.getCount(), is(1));
        // get List<Entry> from the cursor.
        paperEntries = PaperEntry.CONVERTER.getFromCursor(cursor);
        // make sure 1 and only 1 entry was generated from the cursor
        assertThat(paperEntries.size(), is(1));
        // get the inserted Entry
        entry = paperEntries.get(0);
        // check that returned Entry is non-null
        assertNotNull(entry);
        // verify that the returned Entry matches (other than _ID) the inserted Entry.
        assertEquals(entry, testPaperEntry);


        //Authors
        values = testAuthorEntry.getContentValues();
        resultUri = mAuthorMockResolver.insert(mAuthorEntryUri, values);
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(1));
        ArrayList<AuthorEntry> authorEntries = AuthorEntry.CONVERTER.getFromCursor(cursor);
        assertThat(authorEntries.size(), is(1));
        AuthorEntry authorEntry = authorEntries.get(0);
        assertNotNull(authorEntry);
        assertEquals(authorEntry, testAuthorEntry);

        cursor = mAuthorMockResolver.query(resultUri, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(1));
        authorEntries = AuthorEntry.CONVERTER.getFromCursor(cursor);
        assertThat(authorEntries.size(), is(1));
        authorEntry = authorEntries.get(0);
        assertNotNull(authorEntry);
        assertEquals(authorEntry, testAuthorEntry);
    }


    /**
     * Test of Provider ignores junk URIs
     */
    public void testJunkUris() {
        // verify that that query doesn't return on junk URIs.
        Cursor cursor = null;
        try {
            cursor = mPaperMockResolver.query(
                    PaperContract.BASE_URI.buildUpon().appendPath("definitely_invalid").build(),
                    null, null, null, null);
            assertNotNull(cursor);
            // Should have thrown IllegalArgumentException, so now fail.
            fail("cursor.getCount: " + cursor.getCount());
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }


        // verify that that query doesn't return on junk URIs.
        cursor = null;
        try {
            cursor = mAuthorMockResolver.query(
                    AuthorContract.BASE_URI.buildUpon().appendPath("definitely_invalid").build(),
                    null, null, null, null);
            assertNotNull(cursor);
            // Should have thrown IllegalArgumentException, so now fail.
            fail("cursor.getCount: " + cursor.getCount());
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @SuppressWarnings("UnusedAssignment") // Lint is confused w/factory methods.
    public void testDelete() {
        // get ContentValues and insert it into Provider via ContentResolver
        ContentValues paperValues = testPaperEntry.getContentValues();
        ContentValues authorValues = testAuthorEntry.getContentValues();
        Uri insertedUri;

        // query the mock ContentResolver on the Item URI, return all rows.
        int paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        int authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);

        /*
          Test delete all paper rows when only 1 row, with table uri
         */
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, paperValues);
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        assertThat(paperRowsDeleted, is(1));
        Cursor cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(0));

        /*
         * Test delete all author rows when only 1 row, with table uri
         */
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, authorValues);
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        assertThat(authorRowsDeleted, is(1));
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(0));

        /*
         * Test delete 1 paper row when only 1 row, with direct Uri.
         */
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, paperValues);
        paperRowsDeleted = mPaperMockResolver.delete(insertedUri, null, null);
        assertThat(paperRowsDeleted, is(1));
        cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(0));

        /*
         * Test delete 1 author row when only 1 row, with direct Uri.
         */
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, authorValues);
        authorRowsDeleted = mAuthorMockResolver.delete(insertedUri, null, null);
        assertThat(authorRowsDeleted, is(1));
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(0));

        /*
         * Test delete all paper rows when only 1 row, with table uri
         */
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, paperValues);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, paperValues);
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        assertThat(paperRowsDeleted, is(2));
        cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(0));

        /*
         * Test delete all author rows when only 1 row, with table uri
         */
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, authorValues);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, authorValues);
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        assertThat(authorRowsDeleted, is(2));
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(0));

        /*
         * Test delete all paper rows when only 1 row, with table uri
         */
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        paperRowsDeleted = mPaperMockResolver.delete(insertedUri, null, null);
        assertThat(paperRowsDeleted, is(1));
        cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(1));
        List<PaperEntry> paperEntries = PaperEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(paperEntries.get(0), testPaperEntry);

        /*
         * Test delete all author rows when only 1 row, with table uri
         */
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        authorRowsDeleted = mAuthorMockResolver.delete(insertedUri, null, null);
        assertThat(authorRowsDeleted, is(1));
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(1));
        List<AuthorEntry> authorEntries = AuthorEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(authorEntries.get(0), testAuthorEntry);

        /*
         * test delete single paper row when multiple rows, via uri w/number
         */
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        long firstID = ContentUris.parseId(insertedUri);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        Uri insertedUriGoal = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        paperRowsDeleted = mPaperMockResolver.delete(insertedUriGoal, null, null);
        assertThat(paperRowsDeleted, is(1));
        cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(4));
        paperEntries = PaperEntry.CONVERTER.getFromCursor(cursor);
        assertThat(paperEntries.get(0).get_ID(), is(firstID));
        assertThat(paperEntries.get(1).get_ID(), is(firstID + 1));
        assertThat(paperEntries.get(2).get_ID(), is(firstID + 3));
        assertThat(paperEntries.get(3).get_ID(), is(firstID + 4));
        
        /*
         * test delete single author row when multiple rows, via uri w/number
         */
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        firstID = ContentUris.parseId(insertedUri);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        insertedUriGoal = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        authorRowsDeleted = mAuthorMockResolver.delete(insertedUriGoal, null, null);
        assertThat(authorRowsDeleted, is(1));
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(4));
        authorEntries = AuthorEntry.CONVERTER.getFromCursor(cursor);
        assertThat(authorEntries.get(0).get_ID(), is(firstID));
        assertThat(authorEntries.get(1).get_ID(), is(firstID + 1));
        assertThat(authorEntries.get(2).get_ID(), is(firstID + 3));
        assertThat(authorEntries.get(3).get_ID(), is(firstID + 4));

        /*
         * delete single paper row with where clause of _ID from many
         */
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, null, null);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        firstID = ContentUris.parseId(insertedUri);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        insertedUriGoal = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, newPaperEntry().getContentValues());
        paperRowsDeleted = mPaperMockResolver.delete(mPaperEntryUri, " _ID = ?", new String[]{"" + (firstID + 2)});
        assertThat(paperRowsDeleted, is(1));
        cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(4));
        paperEntries = PaperEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(paperEntries.get(0), testPaperEntry);
        assertThat(paperEntries.get(0).get_ID(), is(firstID));
        assertThat(paperEntries.get(1).get_ID(), is(firstID + 1));
        assertThat(paperEntries.get(2).get_ID(), is(firstID + 3));
        assertThat(paperEntries.get(3).get_ID(), is(firstID + 4));

        /*
         * delete single author row with where clause of _ID from many
         */
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        firstID = ContentUris.parseId(insertedUri);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        insertedUriGoal = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, newAuthorEntry().getContentValues());
        authorRowsDeleted = mAuthorMockResolver.delete(mAuthorEntryUri, " _ID = ?", new String[]{"" + (firstID + 2)});
        assertThat(authorRowsDeleted, is(1));
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        assertThat(cursor.getCount(), is(4));
        authorEntries = AuthorEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(authorEntries.get(0), testAuthorEntry);
        assertThat(authorEntries.get(0).get_ID(), is(firstID));
        assertThat(authorEntries.get(1).get_ID(), is(firstID + 1));
        assertThat(authorEntries.get(2).get_ID(), is(firstID + 3));
        assertThat(authorEntries.get(3).get_ID(), is(firstID + 4));
    }

    public void testUpdate() {
        ContentValues paperValues = testPaperEntry.getContentValues();
        ContentValues authorValues = testAuthorEntry.getContentValues();
        Uri insertedUri;
        Cursor cursor;
        ArrayList<PaperEntry> paperEntries;
        ArrayList<AuthorEntry> authorEntries;
        int updatedCount;

        // query the mock ContentResolver on the Item URI, return all rows.
        mPaperMockResolver.delete(mPaperEntryUri, null, null);
        mAuthorMockResolver.delete(mAuthorEntryUri, null, null);

        // setup papers
        mPaperMockResolver.insert(mPaperEntryUri, paperValues);
        mPaperMockResolver.insert(mPaperEntryUri, paperValues);
        insertedUri = mPaperMockResolver.insert(mPaperEntryUri, paperValues);
        cursor = mPaperMockResolver.query(insertedUri, null, null, null, null);
        paperEntries = PaperEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(paperEntries.get(0), testPaperEntry);
        assertNotEquals(paperEntries.get(0), testPaperEntry2);

        // setup authors
        mAuthorMockResolver.insert(mAuthorEntryUri, authorValues);
        mAuthorMockResolver.insert(mAuthorEntryUri, authorValues);
        insertedUri = mAuthorMockResolver.insert(mAuthorEntryUri, authorValues);
        cursor = mAuthorMockResolver.query(insertedUri, null, null, null, null);
        authorEntries = AuthorEntry.CONVERTER.getFromCursor(cursor);
        assertEquals(authorEntries.get(0), testAuthorEntry);
        assertNotEquals(authorEntries.get(0), testAuthorEntry2);

        // test paper update every row.
        mPaperMockResolver.delete(mPaperEntryUri, null, null);
        mPaperMockResolver.insert(mPaperEntryUri, testPaperEntry.getContentValues());
        mPaperMockResolver.insert(mPaperEntryUri, testPaperEntry.getContentValues());
        mPaperMockResolver.insert(mPaperEntryUri, testPaperEntry.getContentValues());
        mPaperMockResolver.insert(mPaperEntryUri, testPaperEntry.getContentValues());
        mPaperMockResolver.insert(mPaperEntryUri, testPaperEntry.getContentValues());

        // test author update every row.
        mAuthorMockResolver.delete(mAuthorEntryUri, null, null);
        mAuthorMockResolver.insert(mAuthorEntryUri, testAuthorEntry.getContentValues());
        mAuthorMockResolver.insert(mAuthorEntryUri, testAuthorEntry.getContentValues());
        mAuthorMockResolver.insert(mAuthorEntryUri, testAuthorEntry.getContentValues());
        mAuthorMockResolver.insert(mAuthorEntryUri, testAuthorEntry.getContentValues());
        mAuthorMockResolver.insert(mAuthorEntryUri, testAuthorEntry.getContentValues());

        // update every paper row to testPaperEntry2
        updatedCount = mPaperMockResolver.update(mPaperEntryUri, testPaperEntry2.getContentValues(), null, null);
        assertThat(updatedCount, is(5));
        cursor = mPaperMockResolver.query(mPaperEntryUri, null, null, null, null);
        paperEntries = PaperEntry.CONVERTER.getFromCursor(cursor);
        // check they all equal testPaperEntry2
        for (int i = 0; i < 5; i++) {
            assertEquals(paperEntries.get(i), testPaperEntry2);
        }

        // update every author row to testAuthorEntry2
        updatedCount = mAuthorMockResolver.update(mAuthorEntryUri, testAuthorEntry2.getContentValues(), null, null);
        assertThat(updatedCount, is(5));
        cursor = mAuthorMockResolver.query(mAuthorEntryUri, null, null, null, null);
        authorEntries = AuthorEntry.CONVERTER.getFromCursor(cursor);
        // check they all equal testAuthorEntry2
        for (int i = 0; i < 5; i++) {
            assertEquals(authorEntries.get(i), testAuthorEntry2);
        }
    }
}
