package org.coursera.androidmooccapstone.provider;

import android.database.Cursor;


import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.coursera.androidmooccapstone.framework.orm.AuthorEntry;
import org.coursera.androidmooccapstone.framework.orm.PaperEntry;
import org.coursera.androidmooccapstone.framework.provider.PaperContract;
import org.coursera.androidmooccapstone.framework.provider.AuthorContract;
import org.coursera.androidmooccapstone.framework.provider.PaperDBAdapter;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/*
*   TEST CONSIDERATIONS
*
* There are no update testing as there will be no db updates in this project.
* */
@RunWith(AndroidJUnit4.class)
public class PaperDBAdapter_AndroidTest {

    private final PaperEntry mEntryA = newPaperEntry();
    private AuthorEntry mEntryB = newAuthorEntry();

    static final public long PAPER_TEST_ID = 123456L;
    static final public String TEST_TITLE = "Lets party like its 1999";
    static final public String TEST_DATE = "20-02-2020";
    static final public String TEST_ABS = "An interesting abstract paragraph";
    static final public String TEST_DOI = "example_doi/202913.3021";

    static final public long AUTHOR_TEST_ID = 123457L;
    static final public String TEST_NAME = "Leonardo";
    static final public String TEST_LASTNAME = "Eras";

    private PaperDBAdapter mPaperDBAdapter;

    static public PaperEntry newPaperEntry() {
        return new PaperEntry(PAPER_TEST_ID, TEST_TITLE, TEST_DATE, TEST_ABS, TEST_DOI);
    }

    static public AuthorEntry newAuthorEntry() {
        return new AuthorEntry(AUTHOR_TEST_ID , TEST_NAME, TEST_LASTNAME, PAPER_TEST_ID);
    }

    static public void checkPaperEntry(PaperEntry testEntry, long id) {
        assertThat(testEntry.get_ID(), Matchers.is(id));
        assertThat(testEntry.getTITLE(), Matchers.is(TEST_TITLE));
        assertThat(testEntry.getDATE(), Matchers.is(TEST_DATE));
        assertThat(testEntry.getABS(), Matchers.is(TEST_ABS));
        assertThat(testEntry.getDOI(), Matchers.is(TEST_DOI));
    }

    static public void checkAuthorEntry(AuthorEntry testEntry, long id) {
        assertThat(testEntry.get_ID(), Matchers.is(id));
        assertThat(testEntry.getNAME(), Matchers.is(TEST_NAME));
        assertThat(testEntry.getLAST_NAME(), Matchers.is(TEST_LASTNAME));
    }

    @Before
    public void setUp() {
        // how to delete database at start. (seems to delete db right now either way...)
        //getTargetContext().deleteDatabase(RssSchema.DATABASE_NAME);
        mPaperDBAdapter = new PaperDBAdapter(getApplicationContext(), true);
        mPaperDBAdapter.open();
    }

    @After
    public void tearDown() {
        mPaperDBAdapter.close();
    }

    /*
    * Testing a simple query on the Paper database, it should be empty now
    * */
    @Test
    public void simpleQueryTestOnPaperTable() {
        assertNotNull(mPaperDBAdapter);
        Cursor cursor = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null,
                null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
    }

    /*
     * Testing a simple query on the Author database, it should be empty now
     * */
    @Test
    public void simpleQueryTestOnAuthorTable() {
        assertNotNull(mPaperDBAdapter);
        Cursor cursor = mPaperDBAdapter.query(AuthorContract.Entry.TABLE_NAME, null, null, null,
                null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
    }

    /*
     * Testing a simple insert on the Paper database
     * */
    @Test
    public void simplePaperInsertTest() {
        // just sanity check
        assertNotNull(mPaperDBAdapter);
        // set start point
        Cursor cursor = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
        // insert mEntryA
        mPaperDBAdapter.insert(PaperContract.Entry.TABLE_NAME, mEntryA.getContentValues());
        Cursor cursor2 = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor2);
        assertThat(cursor2.getCount(), is(1));
    }

    /*
     * Testing a simple insert on the Author database
     * */
    @Test
    public void simpleAuthorInsertTest() {
        // just sanity check
        assertNotNull(mPaperDBAdapter);
        // set start point
        Cursor cursor = mPaperDBAdapter.query(AuthorContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
        // insert mEntryA
        mPaperDBAdapter.insert(AuthorContract.Entry.TABLE_NAME, mEntryB.getContentValues());
        Cursor cursor2 = mPaperDBAdapter.query(AuthorContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor2);
        assertThat(cursor2.getCount(), is(1));
    }

    /*
     * Testing a simple delete on the Paper database
     * */
    @Test
    public void simplePaperDeleteTest() {
        // just sanity check
        assertNotNull(mPaperDBAdapter);
        // set start point
        Cursor cursor = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
        // insert mEntryA
        mPaperDBAdapter.insert(PaperContract.Entry.TABLE_NAME, mEntryA.getContentValues());
        Cursor cursor2 = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor2);
        assertThat(cursor2.getCount(), is(1));
        //Delete mEntryA
        Cursor cursor3 = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor3);
        mPaperDBAdapter.delete(PaperContract.Entry.TABLE_NAME, mEntryA.get_ID());
    }

    /*
     * Testing a simple delete on the Author database
     * */
    @Test
    public void simpleAuthorDeleteTest() {
        // just sanity check
        assertNotNull(mPaperDBAdapter);
        // set start point
        Cursor cursor = mPaperDBAdapter.query(AuthorContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
        // insert mEntryB
        mPaperDBAdapter.insert(AuthorContract.Entry.TABLE_NAME, mEntryB.getContentValues());
        Cursor cursor2 = mPaperDBAdapter.query(AuthorContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor2);
        assertThat(cursor2.getCount(), is(1));
    }

    @Test
    public void inDepthPaperInsertTest() {
        // just sanity check
        assertNotNull(mPaperDBAdapter);
        // set start point
        Cursor cursor = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
        // insert mEntryA
        mPaperDBAdapter.insert(PaperContract.Entry.TABLE_NAME, mEntryA.getContentValues());
        Cursor cursor2 = mPaperDBAdapter.query(PaperContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor2);
        assertThat(cursor2.getCount(), is(1));

        //This object should match with mEntryA
        List<PaperEntry> testItems = PaperEntry.CONVERTER.getFromCursor(cursor2);
        assertNotNull(testItems);
        assertThat(testItems.size(), is(1));
        PaperEntry newItem = testItems.get(0);
        assertTrue(newItem.equals(mEntryA));
    }

    @Test
    public void inDepthAuthorInsertTest() {
        // just sanity check
        assertNotNull(mPaperDBAdapter);
        // set start point
        Cursor cursor = mPaperDBAdapter.query(AuthorContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor);
        assertThat(cursor.getCount(), is(0));
        // insert mEntryA
        mPaperDBAdapter.insert(AuthorContract.Entry.TABLE_NAME, mEntryB.getContentValues());
        Cursor cursor2 = mPaperDBAdapter.query(AuthorContract.Entry.TABLE_NAME, null, null, null, null);
        assertNotNull(cursor2);
        assertThat(cursor2.getCount(), is(1));

        //This object should match with mEntryB
        List<AuthorEntry> testItems = AuthorEntry.CONVERTER.getFromCursor(cursor2);
        assertNotNull(testItems);
        assertThat(testItems.size(), is(1));
        AuthorEntry newItem = testItems.get(0);
        assertTrue(newItem.equals(mEntryB));
    }
}
