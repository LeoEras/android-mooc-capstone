package org.coursera.androidmooccapstone;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewAssertion;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.espresso.contrib.RecyclerViewActions;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class UIBasicTest {
    private View decorView;

    /**
     * Use {@link ActivityScenarioRule} to create and launch the activity under test.
     */
    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule =
            new ActivityScenarioRule<>(MainActivity.class);

    /**
     * Initializing the decorView before working
     * (source: https://stackoverflow.com/questions/28390574/checking-toast-message-in-android-espresso)
     */
    @Before
    public void setUp() {
        activityScenarioRule.getScenario().onActivity(new ActivityScenario.ActivityAction<MainActivity>() {
            @Override
            public void perform(MainActivity activity) {
                decorView = activity.getWindow().getDecorView();
            }
        });
        onView(withId(R.id.erase_data)).perform(click());
    }

    //This one is very basic, it's only to make sure the TextViews are showing up
    @Test
    public void showBasicInstructions() {
        onView(withId(R.id.title)).check(matches(withText(R.string.big_title)));
        onView(withId(R.id.auth)).check(matches(withText(R.string.big_auth)));
        onView(withId(R.id.date)).check(matches(withText(R.string.big_date)));
        onView(withId(R.id.abs)).check(matches(withText(R.string.big_abs)));
    }

    /**
     * Testing a tap on a RecyclerView item, several actions occur
     */
    @Test
    public void tapOnRecyclerViewItem() {
        final String titleTest = "Basic instructions";
        final String authorTest = "Paper Author example";
        final String dateTest = "Paper date published example";
        final String absText = "An amazing abstract";

        //First, we make sure the TextViews are the ones loaded by default, again
        onView(withId(R.id.title)).check(matches(withText(R.string.big_title)));
        onView(withId(R.id.auth)).check(matches(withText(R.string.big_auth)));
        onView(withId(R.id.date)).check(matches(withText(R.string.big_date)));
        onView(withId(R.id.abs)).check(matches(withText(R.string.big_abs)));

        //Now, some data
        gettingData();

        //Now, we make sure the TextViews have changed
        onView(withId(R.id.recyclerView)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.title)).check(matches(not(withText(R.string.big_title))));
        onView(withId(R.id.auth)).check(matches(not(withText(R.string.big_auth))));
        onView(withId(R.id.date)).check(matches(not(withText(R.string.big_date))));
        onView(withId(R.id.abs)).check(matches(not(withText(R.string.big_abs))));
    }

    /**
     * Testing a swipe on a RecyclerView item. This should delete the item in the RV.
     */
    @Test
    public void swipeOnRecyclerViewItem() {
        //Getting some data to delete
        gettingData();
        onView(withId(R.id.recyclerView)).perform(RecyclerViewActions.actionOnItemAtPosition(0, swipeLeft()));
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(5));
    }

    /**
     * Testing network connection. New items should appear in the RecyclerView.
     */
    @Test
    public void onFetchPapersButtonTap() {
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(0));
        gettingData();
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(6));
    }

    /**
     * Testing erase data
     */
    @Test
    public void onDeleteData() {
        //If there's nothing to delete
        onView(withId(R.id.erase_data)).perform(click());
        onView(withText("Nothing to delete"))
                .inRoot(withDecorView(not(decorView)))// Here we use decorView
                .check(matches(isDisplayed()));

        //Getting data to delete
        gettingData();
        onView(withId(R.id.erase_data)).perform(click());
        onView(withText("Data deleted"))
                .inRoot(withDecorView(not(decorView)))// Here we use decorView
                .check(matches(isDisplayed()));
    }


    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = getInstrumentation().getTargetContext();
        assertEquals("org.coursera.androidmooccapstone", appContext.getPackageName());
    }

    private void gettingData() {
        onView(withId(R.id.reload)).perform(click());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Helper class to get a desired number of items in a RecyclerView Adapter.
    public static class RecyclerViewItemCountAssertion implements ViewAssertion {
        private final int expectedCount;

        public RecyclerViewItemCountAssertion(int expectedCount) {
            this.expectedCount = expectedCount;
        }

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();
            assertThat(adapter.getItemCount(), is(expectedCount));
        }
    }
}